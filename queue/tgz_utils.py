import tarfile
import os


class ZipUtils(object):
    def __init__(self, log):
        self.log = log

    def extract(self, source_path, dest_path):
        self.log.info("in extract func")
        tar = tarfile.open(source_path)
        pdf_members = []
        filenames = []
        for tarinfo in tar:
            fname_tokens = os.path.splitext(tarinfo.name)
            if fname_tokens[1] == ".pdf":
                pdf_members.append(tarinfo)
        nFiles = len(pdf_members) // 2  # because of the . file
        tar.extractall(members=pdf_members, path=dest_path)
        tar.close()
        # tgz_dir = os.listdir(dest_path)[0]
        directory = os.fsencode(f"{dest_path}")  # /{tgz_dir}") #'/storage/test-1582694235091-test/lect01.pdf'
        for f in os.listdir(directory):
            filename = os.fsdecode(f)
            if filename[0] != '.':
                filenames.append(filename)
        return nFiles, filenames  # ,tgz_dir

    def archive(self, name, source_dir_path, dest_path):
        with tarfile.open(os.path.join(dest_path, name), 'w:gz') as tar_ref:
            os.chdir(source_dir_path)
            self.log.info(f"compressing {source_dir_path}/*.txt to {dest_path}/{name}")
            for root, dirs, files in os.walk(source_dir_path):
                for f in files:
                    if os.path.splitext(f)[1] == '.txt':
                        tar_ref.add(f)
#
# def archive(self,name, source_dir_path, dest_path):
#        pattern = '*.txt'
#        with tarfile.open(os.path.join(dest_path, name), 'w:gz') as tar_ref:
#            os.chdir(source_dir_path)
#            self.log.info(f"compressing {source_dir_path}/*.txt to {dest_path}/{name}")
#            for root, dirs, files in os.walk(source_dir_path):
#                for file in fnmatch.filter(files, pattern):
#                    tar_ref.add(file, os.path.basename(file))
