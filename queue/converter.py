import textract


class PdfConverter(object):
    def __init__(self, log):
        self.log = log

    def convert(self, f):
        self.log.info(f"converting {f} to text")
        text = textract.process(f, method='pdfminer', encoding="utf-8")
        return text
