import os
from celery import Celery
from kombu import Queue

RABBITMQ_HOST = os.getenv('RABBITMQ_HOST', 'localhost')
RABBITMQ_PORT = os.getenv('RABBITMQ_PORT', 5672)
CELERY_BROKER_URL = f'amqp://{RABBITMQ_HOST}//'
CELERY_RESULT_BACKEND = f'amqp://{RABBITMQ_HOST}//'

celery = Celery('tasks', broker=CELERY_BROKER_URL, backend=CELERY_RESULT_BACKEND) 
celery.conf.task_queues = (
    Queue('queue.task.extract_text_0'),
    Queue('queue.task.extract_text_1'),
    Queue('queue.task.extract_text_2'),
    Queue('queue.task.archive',routing_key='task.archive'),
    Queue('queue.task.unzip',routing_key='tasks.unzip')
)