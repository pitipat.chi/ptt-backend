import os
import uuid
import time
from flask import Flask, jsonify, request
from logzero import logger
from flask_socketio import SocketIO, emit, join_room, rooms, leave_room
from flask_cors import CORS
from worker import celery
# import eventlet
# eventlet.monkey_patch(socket=True)

INSTANCE_NAME = uuid.uuid4().hex

APP_SECRET = os.getenv('APP_SECRET', 'no secret')
RABBITMQ_HOST = os.getenv('RABBITMQ_HOST', 'localhost')
RABBITMQ_PORT = int(os.getenv('RABBITMQ_PORT', 5672))

app = Flask(__name__)
CORS(app, resources={r'/*': {'origins': "*"}}, supports_credentials=True)
app.config['SECRET_KEY'] = APP_SECRET

app.config.update()

socketio = SocketIO(app, message_queue=f"amqp://{RABBITMQ_HOST}", cors_allowed_origins="*", engineio_logger=True)


def get_room():
    return request.sid


@socketio.on('connect')
def connection():
    roomstr = get_room()
    join_room(roomstr)
    logger.info(roomstr)


@socketio.on('disconnect')
def disconnection():
    roomstr = get_room()
    leave_room(roomstr)
    logger.info(roomstr)


@socketio.on('unzip')
def unzip_handler(message):
    roomstr = get_room()
    bucket_name, object_name = message['bucket_name'], message['object_name']
    celery.send_task('task.unzip', args=[bucket_name, object_name, roomstr], kwargs={}, queue='queue.task.unzip', routing_key='task.unzip')


@socketio.on('result')
def celery_result_handler(message):
    bucket_name, object_name = message['bucket_name'], message['object_name']
    room = message['room']
    emit('end_result', {'bucket_name': bucket_name, 'object_name': object_name}, room=room)  # should set room=<room>


@socketio.on('report')
def progress_report_handler(message):
    stage = message['stage']
    room = message['room']
    emit('progress', {'stage': stage}, room=room)


if __name__ == "__main__":
    app.run()

