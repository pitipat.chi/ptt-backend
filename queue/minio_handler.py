import os
import io
from minio import Minio
from minio.error import ResponseError

MINIO_HOST = os.getenv('MINIO_HOST', 'localhost')
MINIO_PORT = int(os.getenv('MINIO_PORT', 9000))
MINIO_ACCESS_KEY = os.getenv('MINIO_ACCESS_KEY', 'minioadmin')
MINIO_SECRET_KEY = os.getenv('MINIO_SECRET_KEY', 'minioadmin')
MINIO_SECURED = bool(os.getenv('MINIO_SECURED', False))


class MinioHandler(object):
    def __init__(self, log):
        self.log = log
        self.client = Minio(f"{MINIO_HOST}:{MINIO_PORT}",
                            access_key=MINIO_ACCESS_KEY,
                            secret_key=MINIO_SECRET_KEY,
                            secure=MINIO_SECURED
                            )

    def put_object(self, bucket_name, object_name, data):
        try:
            io_stream = io.BytesIO(data.encode('utf-8'))
            self.log.info(f"uploading {bucket_name}/{object_name}")
            self.client.put_object(bucket_name, object_name, io_stream, io_stream.getbuffer().nbytes)
        except ResponseError as err:
            self.log.exception("cannot upload object")

    def fput_object(self, bucket_name, object_name, file_path):
        try:
            self.log.info(f"uploading {bucket_name}/{object_name}")
            self.client.fput_object(bucket_name, object_name, file_path)
        except ResponseError as err:
            self.log.exception("cannot upload object")

    def fget_object(self, bucket_name, object_name, file_path):
        # file_path - Path on the local filesystem to which the object data will be written
        try:
            self.log.info(f"downloading {bucket_name}/{object_name}")
            self.client.fget_object(bucket_name, object_name, file_path)
        except ResponseError as err:
            self.log.exception("cannot download object")
