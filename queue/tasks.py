import os
import time
import tempfile
import socketio
from logzero import logger
from celery import Celery,chord
from minio_handler import MinioHandler
from converter import PdfConverter
from tgz_utils import ZipUtils
from shutil import rmtree
# from pymongo import MongoClient
from socketIO_client import SocketIO,LoggingNamespace
from threading import RLock
from random import randint
from kombu import Queue
import shutil

SOCKETIO_HOST = os.getenv('SOCKETIO_HOST', 'localhost')
SOCKETIO_PORT = os.getenv('SOCKETIO_PORT', 5000)
RABBITMQ_HOST = os.getenv('RABBITMQ_HOST', 'localhost')
RABBITMQ_PORT = os.getenv('RABBITMQ_PORT', 5672)
MONGO_HOST = os.getenv('MONGO_HOST', 'localhost')
MONGO_PORT = os.getenv('MONGO_PORT', 27017)
CELERY_BROKER_URL = f'amqp://{RABBITMQ_HOST}//'
CELERY_RESULT_BACKEND = f'amqp://{RABBITMQ_HOST}//'

celery = Celery('tasks', broker=CELERY_BROKER_URL, backend=CELERY_RESULT_BACKEND)

# client = MongoClient(f'mongodb://{MONGO_HOST}:{MONGO_PORT}/')
# db = client.p4_database
# user_requests = db.user_requests

# sio = socketio.Client()
# sio.connect(f"http://{SOCKETIO_HOST}:{SOCKETIO_PORT}")
socketIO = SocketIO(f"{SOCKETIO_HOST}", f"{SOCKETIO_PORT}", LoggingNamespace)

minio = MinioHandler(logger)
pdf_utils = PdfConverter(logger)
zip_utils = ZipUtils(logger)
storage_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir) + "/storage")
if not os.path.exists(storage_dir):
    os.makedirs(storage_dir)

lock = RLock()

celery.conf.task_queues = (
    Queue('queue.task.extract_text_0'),
    Queue('queue.task.extract_text_1'),
    Queue('queue.task.extract_text_2'),
    Queue('queue.task.archive',routing_key='task.archive'),
    Queue('queue.task.unzip',routing_key='tasks.unzip')
)


def route_task(self, name, args, kwargs, options, task=None, **kw):
    return f"queue.{task}"

def print_active_queues():
    print("printing active queues")
    d = celery.control.inspect().active()
    lst = d[list(d.keys())[0]]
    i = 1
    for item in lst:
        print(i, item['name'])
        i+=1


@celery.task(name='task.archive')
def archive(object_names, bucket_name=None, dest_folder=None, archive_name=None, user_id=None):
    print("in archive")
    socketIO.emit('report', {'stage': "COMPRESSING", 'room': user_id})

    print_active_queues()
    # print("active == ")
    # print(celery.control.inspect().active())
    for text_filename in object_names:
        minio.fget_object(bucket_name, text_filename, dest_folder+ "/" + text_filename)
    source_folder = dest_folder

    zip_utils.archive(archive_name, source_folder, dest_folder)
    minio.fput_object(bucket_name, archive_name, f"{dest_folder}/{archive_name}")
    shutil.rmtree(dest_folder)
    #socketIO.emit('result', {'bucket_name': bucket_name, 'object_name': archive_name, 'room': user_id})
    socketIO.emit('report', {'stage': "DONE", 'room': user_id})
    socketIO.emit('result', {'bucket_name': bucket_name, 'object_name': archive_name, 'room': user_id})

@celery.task(name="task.extract_text_0")
def extract_text_0(bucket_name, object_name, user_id):
    socketIO.emit('report', {'stage': "CONVERTING", 'room': user_id})
    print("in extract_text0")
    tempf = tempfile.NamedTemporaryFile(mode="w",encoding='utf-8',suffix=".pdf")

    new_obj_name = os.path.splitext(object_name)[0] + ".txt"
    # text_file_path = f"{dest_path}/{new_obj_name}"
    minio.fget_object(bucket_name, object_name, tempf.name)

    text = pdf_utils.convert(tempf.name).decode('utf-8')
    # text.getbuffer().nbytes
    minio.put_object(bucket_name, new_obj_name, text)

    return new_obj_name

@celery.task(name="task.extract_text_1")
def extract_text_1(bucket_name, object_name, user_id):
    socketIO.emit('report', {'stage': "CONVERTING", 'room': user_id})
    print("in extract_text1")
    tempf = tempfile.NamedTemporaryFile(mode="w",encoding='utf-8',suffix=".pdf")

    new_obj_name = os.path.splitext(object_name)[0] + ".txt"
    # text_file_path = f"{dest_path}/{new_obj_name}"
    minio.fget_object(bucket_name, object_name, tempf.name)

    text = pdf_utils.convert(tempf.name).decode('utf-8')
    # text.getbuffer().nbytes
    minio.put_object(bucket_name, new_obj_name, text)

    return new_obj_name

@celery.task(name="task.extract_text_2")
def extract_text_2(bucket_name, object_name, user_id):
    socketIO.emit('report', {'stage': "CONVERTING", 'room': user_id})
    print("in extract_text2")
    # print(celery.control.inspect().active())
    tempf = tempfile.NamedTemporaryFile(mode="w",encoding='utf-8',suffix=".pdf")

    new_obj_name = os.path.splitext(object_name)[0] + ".txt"
    # text_file_path = f"{dest_path}/{new_obj_name}"
    minio.fget_object(bucket_name, object_name, tempf.name)

    text = pdf_utils.convert(tempf.name).decode('utf-8')
    # text.getbuffer().nbytes
    minio.put_object(bucket_name, new_obj_name, text)

    return new_obj_name

extract_tasks = [extract_text_0, extract_text_1, extract_text_2]

def get_free_extract_tasks():
    print_active_queues()
    inspect_dict = celery.control.inspect().active()
    print("inspect_dict")
    print(inspect_dict)
    celery_keys = ["celery@extract0","celery@extract1","celery@extract2"]#list(inspect_dict.keys())[0]
    extract_text_available = [True, True, True]
    for i in range(3):
        actives_list = inspect_dict[celery_keys[i]]

        for active_dict in actives_list:
            task_name = active_dict['name']  # == "task.unzip.rep1"
            if "extract_text" in task_name:
                #idx = int(task_name[-1])
                extract_text_available[i] = False
    return [i for i in range(3) if extract_text_available[i]]


@celery.task(name="task.unzip")
def unzip(bucket_name, object_name, user_id):
    print("in zip")
    print_active_queues()
    # print("celery.control.inspect().active() == ")
    # print(celery.control.inspect().active())
    socketIO.emit('report', {'stage': "EXTRACTING", 'room': user_id})
    # celery.app.control.Inspect
    tmpf = tempfile.NamedTemporaryFile()
    # new_obj_name = os.path.splitext(object_name)[0] + ".txt"

    # write .tgz in <tmpf.name> path
    minio.fget_object(bucket_name, object_name, tmpf.name)

    buck_obj_name = f"{bucket_name}-{os.path.splitext(object_name)[0]}"
    # extract .tgz to multiple .pdfs and write to a folder of name
    dest_path = f"{storage_dir}/{bucket_name}-{os.path.splitext(object_name)[0]}"
    npdfs, filenames = zip_utils.extract(tmpf.name, dest_path)  # , tgz_dir
    # request_data = {'user_id': user_id, 'file_quantity': npdfs, 'file_names': filenames}
    # request_id = user_requests.insert_one(request_data).inserted_id

    # Todo: find which extract text copies to send work to
    lock.acquire()
    free_extract_tasks = get_free_extract_tasks()
    lock.release()
    #empty

    if not free_extract_tasks:
        i_th_task = randint(0,2)
    else:
        i_th_task = free_extract_tasks[0]
    print("free_extract_tasks == ")
    print(free_extract_tasks)
    print("bucket name, queue")
    print(bucket_name, i_th_task)
    get_text_tasks = []
    # upload pdf files to minio
    for filename in filenames:
        minio.fput_object(bucket_name, filename, f"{dest_path}/{filename}")
        #get_text_tasks.append(extract_text.s(bucket_name, filename, user_id))  # , f"{dest_path}/{tgz_dir}"))
        get_text_tasks.append(extract_tasks[i_th_task].s(bucket_name, filename, user_id).set(queue=f'queue.task.extract_text_{i_th_task}'))#, f"{dest_path}/{tgz_dir}"))
    # Todo: delete files on sys (directory:  f"{dest_path})
    archive_params = {'bucket_name': bucket_name, 'dest_folder': f"{dest_path}/{filename}-texts",
                     'archive_name': f"{buck_obj_name}-texts.tgz",'user_id': user_id}
    archive_callback = archive.s(**archive_params)
    result = chord(get_text_tasks)(archive_callback.set(queue="queue.task.archive"))
