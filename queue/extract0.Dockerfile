FROM python:3.7

COPY requirements.txt /app/requirements.txt

RUN pip3 install --no-cache-dir -r /app/requirements.txt

COPY . /queue
WORKDIR /queue

ENTRYPOINT celery -A tasks worker -c 3 -Q queue.task.extract_text_0 -n extract0 --loglevel=info